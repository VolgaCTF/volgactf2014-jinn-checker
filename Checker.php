<?php
require('vendor/autoload.php');

use Femida\Checker;
use Femida\Result;


class SolarssChecker extends Checker{
	protected $ip;
	protected $port;
	protected $flag;
	protected $flag_id;
	protected $connection;


	public function construct($endpoint, $flag_id, $flag){
		list($this->ip, $this->port) = explode(':', $endpoint);
		$this->flag_id = $flag_id;
		$this->flag = $flag;
		$this->hash = substr(sha1($flag_id),0,20);
		$this->connection = NULL;
	}

	public function isUp(){
    $this->connection = @fsockopen($this->ip, $this->port);
    if($this->connection == NULL)
      return FALSE;
    stream_set_timeout($this->connection, 10);
    $intro = 
"
                                          ..                               
                                         dP/\$.                             
                                         \$4\$\$%                             
                                       .ee\$\$ee.                            
                                    .eF3??????\$C\$r.        .d\$\$\$\$\$\$\$\$\$\$\$e. 
 .zeez\$\$\$\$\$be..                    JP3F\$5'\$5K\$?K?Je\$.     d\$\$\$FCLze.CC?\$\$\$e 
     \"\"\"??\$\$\$\$\$\$\$\$ee..         .e\$\$\$e\$CC\$???\$\$CC3e\$\$\$\$.  \$\$\$/\$\$\$\$\$\$\$\$\$.\$\$\$\$ 
            `\"?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$b \$\$\"\$\$\$\$P?CCe\$\$\$\$\$F 
                 \"?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$b\$\$J?bd\$\$\$\$\$\$\$\$\$F\" 
                     \"\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$d\$\$F\"           
                        \"?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\"...           
                            \"?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$F \"\$\$\"\$\$\$\$b         
                                \"?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$F\"     ?\$\$\$\$\$F         
                                     \"\"????????C\"                          
                                     e\$\$\$\$\$\$\$\$\$\$\$\$.                        
                                   .\$b CC\$????\$\$F3eF                       
                                 4\$bC/%\$bdd\$b@\$Pd??Jbbr                    
                                   \"\"?\$\$\$\$eeee\$\$\$\$F?\"   
Hello, Genie!
I`m your new master, you must serve me.
I think, that i can share some treasure with you.
But I have to check your abilities.
First of all, you will help me with math.
Then, you will have to recognise a person.
Later you will prove your programming skills
Are you ready? [y/n]
";
    fwrite($this->connection, $intro);
    $response = trim(fread($this->connection, 10));

    return $response === "y" ? TRUE : FALSE;
  }

  private function step1(){
    //some math
    $actions = array("+", "-", "*", "/", "**");
    //numbers

    while(1){
      for ($i=0; $i < 20; $i++) { 
        $numbers[] = mt_rand(1,5);
      }
      $brackets = 0;
      $line = "";

      $pow = 0;
      $t = 4;
      for($i=0; $i < 19; $i++) {
        $a = mt_rand(0, $t);
        if($a == 4){
          $pow++;
          if($pow == 2)
            $t = 3;
        }
        $line .= $numbers[$i] . $actions[$a] ;
      }
      $line .= $numbers[19];
      exec("python -c \"print $line\"", $out);
      //echo $line;
      if(strlen($out[0]) < 30)
        break;
    }
    

    fwrite($this->connection, "Calc this: $line\n");
    sleep(1);
    $res = trim(fread($this->connection, 100));

    return $res == $out[0];
  }

  private function step2(){

    $recon = array(
      "Aladdin" => "                             ..........  .uoeeWWeeeu.                     
                      .:::::::::::::::::: \"?\$\$\$\$\$\$\$\$\$\$\$c                  
                    ```....:::::::::::::::::`\"\$\$\$\$\$\$\$\$\$\$\$e.               
               ..:::::::::::```.::::::::::::::.`\"??\$\$\$\$\$\$\$b               
           ::::::::::::::` .:::::::` ::::::`` :::::::  `\"?\$               
        .:::::::::::::``.::::::::` .:::`` ::::::::::::::::.               
       :::::`.:::::::  ::::::::`  ::` . `:::::::::::::::::::.             
     ::::` ::::::::`` :::::::`   .ue@\$\$ `:::::::::::::::::::::            
     ::` .:::::::``z, :::::`.e\$\$\$\$\$\$\$\$\$\$.`::::::::::::::::::::            
    :` :::::::``,e\$\$\$r`::: \$\$\$\$??'     `?b_ `::::::::::::::::::           
   ' :::::::` ,?'   `?b,_` R\$'     .,,.   `\"iu ````:::::::::::::          
    ::::::`   <  .,.   `?e. \$\$eeee\$\$\$F???\$ee,3\$c   ..````::::::::.        
   ::::::  ::  R\$\$\$\$\$\$\$\$e4\$ \$\$\$\$\$\$\$\"e     3\$\$\$\$\$.:.  ``:::::::::``:.      
  `:::::: :::::`\$F.   \"?FJ\$d\$\$\$\$\$\$'\$L~. . .\$\$\$\$\$L`!!~eec``::::::::.       
  `::::::.```::.\"\"\$'     \$\$\$\$\$\$\$\$\$.\$\$bKUeiz\$\$\$\$\$\$'!~ \$\$3\$  `::::::::      
     ``````` ..: 3`beeed \$\$\$\$\$\$\$\$\$\$e\$\$Ned\$\$\$\$\$\$\$\$'u@z\$5\$ ::: `:::::::     
             ::: ^\$NeeeP \$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\"\$NNeP ::::::`::::::     
            .:::: \$\$\$\$\$F \$\$\$\$\$\$\$?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$ \$\$F .:::::::::::::      
           : .::: ?\$\$\$\$\$ \$\$\$\$\$\$\$?c\$\$\$\$\$\$\$\$\$\$\$\$\$\$F,e :::::::::::::``       
             ::::,`\$\$\$\$\$\$,)?\$\$X\$\$\$\$\$\$\$\$\$\$\$?\$\$\$\$\$ \$\$ :::::::::::``         
             ::':: \"\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$P?\" .d\$\$\$\$\$F,\$\$ :::::::::`            
             `  :::.\"\$\$\$EC\"\"???\"?Cz=d\"ud\$\$\$\$\$\$\".\$\$\$ :::::`:`              
                `:::.`?\$\$\$bu. 4\$\$\$??Le\$\$\$\$\$\$P\".\$\$\$\$ ::```                 
                  `::: `?\$\$\$P\$beee\$\$\$\$\$\$\$\$P\".d\$\$\$\$\$                       
                   `:`   `?\$\$eJCCCNd\$\$\$\$\$F.z\$\$\$\$\$\$\$ \$u.                   
                          u`?\$\$\$\$\$\$\$\$\$\$F z\$\$\$\$\$\$\$\$Pu`\$\$\$c.                
                         c^\$bu?R\$\$\$\$F\"ue\$\$\$\$\$\$\$\$\$\$ \$ \$\$\$\$\$\$bc.            
                       e\$\$ \$\$\$e,`\"\",e\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$ \$\$\$\$\$\$\$\$\$\"bc.        
                    .e\$\$\$\$ '\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$ \$\$\$\$\$\$\$\$'d\$\$\$\$\$be.   
                ..e\$\$\$\$\$\$EJ,?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$'\$\$\$\$\$\$\$\"d\$\$\$\$\$\$\$\$\$\$be 
 \n",
      "Genie" => "                                                                          
                          `  `  -  .  _                                   
      '  _                  `            .                                
       .     -  .  .  .  . - '             .                              
                                            `.                            
          ` .  .         .  .  .  .           .                           
                  `   `              `  `.      `                         
                . - .                   '         `.           .nMf       
               .      `               '  iXMMMMMM            .MMMM        
          ..       ..   `     .xxnMM' ,nMMMMMMMMMX  `      :MM XM         
           \"MMM> <MMMMMi  !MMMMMMMM',MMMMMMMMMMMMMX  `   .MMf xM'         
            `?MM.`MMMMMMMX.MMMMMMMhHMMMMMMMMMMMMMMM<MM  :MMP .MM          
              `Mh ?MMMMMMMMMMMMMMMMMP\"\"\"\"?MMMMMMMMMMMf 4MMM  MM>          
                Mh ?MMMP`\"\"MMMMMMMMM'.d\$\$Nu.`?MMMMMMM  MMMf dMM>          
                 \"k MMP e\$b.'?MMMMf u\$\$\$\$\$\$\$b `MMMMMMMM`MM  `MM>          
                  ?  M d\$\$\$\$b.`MMM '\$\$\$\$\$\$\$\$\$b 'MMMMMMM 'M <  ML          
                   \ 4 3\$\$\$\$\$\$b`MX \$F\"\"\$\$\$\$\$\$\$  MMMMMMMM  \dk 4M          
                  . . L'\$\$F   \$ `X \$    \$\$\$\$\$E 4MMMMMMMMM. \"f MMk         
             .x*\"\"` .dM. ?\$ouu\$ 'M \"\$ed\$\$\$\$\$\$ '\" .:dMMMMMM. ?MMMMr        
            MM>4  xMMMhx. `?\$\$\$ 4MM ?\$\$\$\$\$P  xMMMMMMMMMMMMM  MMMMP        
            ?MM> MMMMMMMMMMMn  dMMMMx`?\$\".dMMMMMMMMMMMM???M>              
                 'MP\"\"\"\"\"\"\"\"` HMMMMMMM~.dMMMMMMMMP\"\" .r xMM               
                  `MMM < 'F :MMMMMMMMMMMMMMMP\"`  d\$ JMf MMM .             
                MM 'M :M '  MMMMMMMMMMMMnnndM\"  d\$F MM  4M  M:            
              .MMMM  .MM  L 4MMMMMMMMP\"MMP\" .z\$ \$\$k MM> 'M 4MM:           
             .MMMMP .MMM  \$r'MMMMMMP  ` .zd\$\$\$\$ \$\$k MMX  ? 4MMM:          
            MMMMMf XMMMf 4\$\$ 'MMMM  \$\$\$ 3\$\$\$\$\$\$k\"\"  4MM.   'MMMMh         
          .MMMMP  dMMMM   ^\" o \"\" u\$\$\$\$ 4??\"\"        MMMr  `MMMMMM:       
         dMMMMf :MMMMM\"         `\"\"``           ...; 'MMM:  `MMMMMML      
       xMMMMP\" :MMMMMf               ..;;;  .i!!!!!!!  MMMM   \"MMMMMM:    
     xMMMMMf  :MMMP\"\"             <!!!!!` !!!!!!!'`  r MMMMh   `MMMMMMh   
   :MMMMMP   xMMM\".u.          :!!!!!!!!i!!''` .u  \$\$F MMMMMh    ?MMMMMMx 
 .dMMMMM\"   XMMP  \$\$\$\$h .     '''''''```  .ue\$\$\$\$R \$F.MMMMMMMM    ?MMMMMMM 
MMMMMMM     MMM. '?\$\$\$'J\$\$\$\$\$\$bbr.d\$\$\$\$\$\$;4\$\$\$\$\$\$\$  :MMMMMMMMM     MMMMMMM 
MMMMMP     MMMMMMM:  \" \$\$\$\$\$\$\$\$\$'8\$\$\$\$\$\$\$k`\$\$\$P\" .nMMMMMMMMMMM     MMMMMMM 
MMMMM      MMMMMMMMMMn:. `\"???\$\$ \$\$\$\$\$????'\" .xdMMMMMMMMMMMMM\"     MMMMMMM 
MMMM'     `MMMMMMMMMMMMMMMMnn...........ndMMMMMMMMMMMMMMMMMM       MMMMMMM 
MMMM      `MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\"       XMMMMMMM 
MMMM       MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\"        nMMMMMMMM 
MMMMk      `MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMP\"          MMMMMMMMMM 
MMMMMM.      \"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMP\"            uMMMMMMMMMMM 
MMMMMMMM.      `\"\"MMMMMMMMMMMMMMMMMMMMMMMMMP\"\"             .dMMMMMMMMMMMMM 
MMMMMMMMMh:         `\"\"\"\"???MMMMMMMPP\"\"\"                .nMMMMMMMMMMMMMMMM 
MMMMMMMMMMMMx.                                      .xMMMMMMMMMMMMMMMMMMMM 
MMMMMMMMMMMMMMMr                                :dMMMMMMMMMMMMMMMMMMMMMMMM 
MMMMMMMMMMMP\"`                         ..:nnMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 
MMMMMMM\"\"                  ...xnnnnHMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 
MMMM\"         ....ndMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 
MMM     xnMMMM\"\"`\"\"\"MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 
MMM   'MMMMMM     n  'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 
MMM.   \"?MMMMMh:.xM>  :MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 
MMMMMn    '\"\"???\"\"   :MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 
MMMMMMMMhnx.......nHMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n",
      "Jafar" => "
                      :   .                                               
                      ::  `::.                                            
                      :::: `::::                                          
                     .:::::  ::::::                                       
                     :::::::  `::::::                                     
                    .::::::::  :::::::                                    
                    ::::::::::  ::::::::                                  
                    :::::::::::  ::::::::                                 
                    ::::::::::::  ::::::::                                
                     `:::::::::::  :::::::  ?\$\$e                          
                   uu   `:::::::::  ::::::  `\$\$\$\$bc                       
                ,d\$\$\$\$u   :::::::::  ::::::  ?\$\$\$\$\$\$bc                    
              x\$\$\$\$\$\$\$\$\$u  `::::::::  :::::: `\$\$\$\$\$\$\$\$\$b                  
            d\$\$\$\$\$\$\$\$\$\$\$\$\$b   :::::::  :::::  \$\$\$\$\$\$\$\$\$\$\$b                
          z\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$b   ::::::  ::::  `\$\$\$\$\$\$\$\$\$\$\$\$b              
        .d\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$L  `::::  `::::  \$\$\$\$\$\$\$\$\$\$\$\$\$b.            
       s\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$L `::::  ::::  ?\$\$\$\" ::::`\"?\$\$b           
      d\$\$\$\$\$\$\$\$\$\$\$\$F\"\"    \"??\$\$\$\$b. `::: `:::: `\$E .:::::::: \"Rb          
     d\$\$\$\$\$\$\$\$\$\$\"\"  ::::::::  \"\$\$\$\$i  ::. `:::  \$> ::'  `::::: \"L         
    d\$\$\$\$\$\$\$\$\$\" .:::::`````:::. \"\$\$\$\$  ::. `::  \$F :: MMMx  `:: `L        
   4\$\$\$\$\$\$\$\$\" ::::`   .nnnx `:::  \$\$\$\$  ':  ::  \$\$ :: MMMMMM  `:          
   \$\$\$\$\$\$\$R  ::`  xMMMMMMMMMh `~~ \"\$\$\$\$i `. ::  \$\$ :` MMMMMMMx `:         
  '\$\$\$\$\$\$\$ ::` :MMMMMMMMMMMMMM '~: T\$\$\$\$r `  :  \$\$ : 4MMMMMMMML `:        
  '\$\$\$\$\$\$ .:  MMMMMMMMMMMMMMMMM  ~  \$\$\$\$\$>      ?P : MMMMMMMMMM: :        
  <\$\$\$\$\$F :` MMMMMMMMMMMMMMMMMMM  : ^\$\$F  <~  `.. ' dMMMMMMMMMMM          
  <\$\$\$\$\$' :  MMMMMMMMMMMMMMMMMMMM: ~: \" '~  , .  . `MMMMMMMMMMMM:         
  '\$\$\$\$\$  : 'MMMMMMMMMMMMMMMMMMMMMn. `  '  >   `  . MMMMMMMMMMMMM         
  '\$\$\$\$\$L : 'MMMMMMMMMMMMMMMMMMMMMMMMMk '>  ~~~~  : MMMMMMMMMMMMM         
   \$\$\$\$\$\$ :. MMMMMMMMMMMMMMMMMMMMMMMMMMh  `   ~~ ~ :MMMM\"   MMMMT         
   ?\$\$\$\$\$: : \"MMMMMMMMMMMMMMMMMMMMMMMMMMMn  `   ~ uuu.      MMMM          
   `\$\$\$\$\$\$ `. MMMMMMMMMMMMMMMMMMMMMTTMM\" .e\$\$bee\$\$\$\$\"   .   MMT           
    ?\$\$\$\$\$b : `MMMMMMMMMMMMMMMMMMf      \"\$\$\$\$\$\$\$\$\$\$\$\$  :F  d\"\"            
     ?\$\$\$\$\$k : `MMMMMMMMMMMMMMMMM        \"\$\$\$\$\$\$\$\$\$?\$  ~.e.               
      ?\$\$\$\$\$  . TMMMMMMMMMMMMMMMM  :\$\$c.  `\"\$\$\$\$\$\$\$ \$  4P\" :              
       \"\$\$\$\$\$.'  TMMMMMMMMMMMMMMM   \$\$\$\$F    \"?\$\$\$P d>'     .             
         ?\$\$\$\$>   TMMMMMMMMMMMMM  h.  ^\" \"\"       .d\$b '~.r \$             
           \"\$\$\$L'\ \"MMMMMMMMMMT  MMf d\$F    .u\$\$\" \$\$\$\$b.^  \$\"             
             \"?\$L ` `TMMMMMMMM  HMf 4\$\$\$.-xuC.  d\$\$\"\"?\$\$\$\$c               
                 ^=   `MMMMMM  MMMM . `?\$\$eeeed\$\$\$ 4\$\$\$\$\$\$\$b .            
                        \"TTM' dMMMT MM  .\"\$\$\$\$\$\$\$\$ ..\"?\$\$\$\$F M,           
                             dMMMM :MM  \$i?\$\$\$\$\$F\"u\$\$  \"?\$\$' MM.          
                            dMMMMT MMM  \$\$\$\$\$F\".u\$\$\$F d ..  dMMM.         
                           MMMMMM nMMM  3\$\$\$ . 3\$\$\$\$F'\$ \$\$k'MMMMMx        
                          MMMMMM\" MMMM  3\$\$\"'\$b ?\$\$\$  R '\$\$L MMMMMk       
                         dMMMMM\" MMMM>  3\$E .'\$b.'\"\"  .k   ^  TMMMMx      
                        MMMMMMT <MMMM H: Rk 4k ?\$\$\$\$\$\$\$\$\$\$\$\$\$N TMMMMh     
                      :MMMMMMM :MMMMM MMk.`\" 9\$b,.`\"\" ..uuu '\".MMMMMMM    
                     :MMMMMMM' MMMMM>'MMMMMh. `\"?\$\$\$\$\$\$\$\$\$F .\"TMMMMMMMM:  
                    nMMMMMMMM nMMMMM 4MMMMMMMMM.  \"?\$\$\$\$\$\$\$\$\$\$i  \"TMMMMM. 
                   MMMMMMMMM':MMMMMM NMMMMMMMMMMMMnx   \"??\$\$\$\$\$\$    TMMMMM 
                 :MMMMMMMMM' MMMMMMT MMMMMMMMMMMMMMMMMn..    `\"\"    xMMMMM 
               .MMMMMMMMMM' nMMMMMM> MMMMMMMMMMMMMMMMMMMMMx.       dMMMMMM 
              :MMMMMMMMMMM nMMMMMMM <MMMMMMMMMMMMMMMMMMMMMM      MMMMMMMMM 
            .MMMMMMMMMMMM :MMMMMMMM 'MMMMMMMMMMMMMMMMMMMMM       `\"\"\"\"?MMM 
           :MMMMMMMMMMMM  MMMMMMMMM 'MMMMMMMMMMMMMMMMMMMMMMnnnnnn=--+nr )M 
          :MMMMMMMMMMMM' MMMMMMMMMMr'MMMMMMMMMMMMMMMMMMMMMMMMMM\" nn.-\" :MM 
         HMMMMMMMMMMMM' MMMMMMMMMMM> MMMMMMMMMMMMMMMMMMMMMMMMMMMnnxxnMMMMM 
 \n",
      "Jasmine" => "
                            .::::::`.::::::::::..                         
                         .''``````.:::::::::::::::::                      
                      .:',ere\$ze c :::::::::::::::::::                    
                    ,'` e\$\$\$\$\$\$\$-K eeeu...`````:::::::::                  
                   .zd>^leeu^R\$%:FJ\$\$\$\$\$\$\$\$\$\$e.. ``::::::                 
               .ed\$\$\$\$4\$\$\$\$\$P-u@\" \"\"?????R\$\$\$\$\$\$\$hc. ``:::                
             .e\$\$F\"..: P??7loF .:::::::::::..\"\"\"?R\$\$\$e. `:.               
            zF\".:::::::`\"\"\"'.:::::::::::::::::::::.`\"?\$\$e.`               
            .::::::::::::':::::::::::::::::::::::::::::.`\"=.              
          .:::::::::::::` `:::::::::::::::::::::::::::::::..              
       .:::::::::::::` ud\$ec.  ``:::::::::::::::::::::::::::::.           
     .:::::::::::`` .zd\$\$\$\$\$\$\$ec..  ```::::::::::::::::::::::::::         
   .:::::::::::`    \"??\$\$\$\$\$\$\$\$\$\$\$P        ``::::::::::::::::::::::       
  ::::::::::` .. \$\$*.  ^\$\$\$\$\$\$\$\$\$\$ .e\$**\"\" =e=..  ``::::::::::::::::.     
  ::::::::: :::.  . ^  \$\$P\$\$\$\$\$\$\$\$\$\$F  .'\$\$\$N4\$\$L'::.  `:::::::::::::::   
  `::::::` :::::: \$    '\$\$4\$\$\$\$\$\$\$\$% - : \$\$\$F\$\$\$\$u`::::::. `:::::::::::.  
   :::::: :::::: .^m.-.e.\$'\$\$\$\$\$\$\$P.  -)z\$?Cd\$\$\$\$\$u `:::::::. `:::::::::  
    `::::::::::: J\$buFh52d4\$\$\$\$\$\$\$LcccccCz\$\$\$\$\$\$\$\$\": `::::::::..::::::::: 
      `::::::::: \$\$\$\$\$\$\$\$PJ\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$F.d\$\$. `::::::::::::::::` 
        `::::::  ?\$\$\$\$\$\$\$F\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$P x\$\$\$\$\$\$L `::::::::::::::  
          ``:: dN ?\$\$\$\$\$\$N2\$\$?3\$\$\$\$\$\$\$\$\$\$\$\$\$\$P dP\\$\$\$\$\$\$\$u `::::::::::::  
            `:'\" \$.`R\$\$P???\$\$P??????\"\$\$\$\$\$\$\$\$ 9\".d\$\$\$\$\$\$\$\$b.`::::::::::   
            ::   R\$\$. ?\$\$r  `..;; . \$\$\$\$\$\$\$F\"'\$'\$J?\$\$\$\$\$\$\$f   ::::::::    
            `:.^\"\"\"\"\"\".\"?\$`niiiodfu\$\$\$\$\$F\"z\$\$ ^\"\"~\"\"\"\"`..::::  ::::::     
              `::::::::::`?beCCbe\$\$\$\$\"\"cd\$\$\$\$i .`::::::::::::. `:::`      
                ``:::::::::`?\$\$\$P\"',cd\$\$\$\$\$\$\$\$r?k :::::::::::`.:::`       
                   ```::::::::: 4\$\$\$\$\$\$\$\$\$\$\$\$\$\"d\$ ``::::::::::::`         
                         ``::: . \$\$\$\$\$\$\$\$\$\$\$\$fJ\$F4\$e.``:::::::`           
                              dR `\$\$\$\$\$\$\$\$\$\$\"x\$\$ d\$\$\$\$eu,.```             
                          .e\$ \$E b?\$\$\$\$\$\$\$\".d\$\$ d\$\$\$\$\$\$\$\$\$\$\$e..           
                    ..ee\$\$\$\$\$ \$\$k`\$\$\$\$\$\$\".d\$\$\".\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$hec.     
               .ze\$\$\$\$\$\$\$\$\$\$\$b\"\$\$heeeeeud\$R\" e\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$e. 
           z\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$h`?c\"\"\"\"J\$R z\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$ 
      .ee\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$hc\"xJ>=\".zd\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$ 
    .d\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$he\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$ 
 \n",
      "Abu" => " 
                                                    ..ccCCCCCCCc..        
                                             , .ccCCCCCCCCCCCCCCCCC>      
                                          .cCCCCCCCCCCCCCCC. ``CCCCc      
          _                            .cCCCCCCCCCCCCCCCCCCCCCc `?CCCc    
        +'  ` -                   .ccCCCCCCCCCCCCCCCCCCCCCCCCCCCc  `CCc   
      ,'      <!ii;...   .....c '     ``CCCCL ``CCCCCC`CCCCCCCCCCCc `CCc  
     ;         `!!!!' .cCCCCCC'`  .cCC''         `'CCC  `CCCCCCCCCCC  `C  
    ,    :i!!!!!!!  :CCCCCC'  ,cCC'`  .ccCCCCC  C: 'CCC    `CCCCCCCCC  '  
   .      !!!!!!' .CCCCCCC .cCCC'  .cCCCCCCCCCC CCC  CC      `CCCC`CC     
   :       `!!!  cCCCCCC' cCCC'  cCCCCCCCCCCCCC CCCCc C> <>   `CCCC C     
     ~ -:ii!!! :CCCCCCC  CCCC  cCCCC'`'CCCCCCC' CCCCC   :CC    `CCC `     
          !!! :CCCCCCCC CCC' ,CC'' ..c..'`CCCC  CCCC'   CCC     CCC       
           ` .CCCCCCCC; CC; ,C' .d\$\$\$\$\$\$\$N. `C .CCC :> :CCC     `C'       
             CCCCCCCCC':CC ,C .d\$\$\$\$\$\$\$\$\$\$\$b ` CC' ''  CCCC      '        
            '''CCCCCCC 'C> C .\$\$\$\$\$\$\$\$\$\$\$\$\$\$L ,C' .e\$\$L CC'               
       ;;CCCCC;;.  CCC>'C ;  \$\$\$\$F  `\"\$\$\$\$\$\$\$ C' d\$\$\$\$\$L `                
     ;CCCC...``~CCc CC> C C 4\$\$\$\"     `\$\$\$\$\$\$ ' d\$?\$\$\$\$\$                  
     CCCCCCCCCCCc `C CC C>' 4\$\$\$      d\$\$\$\$\$F  /   `\$\$\$\$                  
     CCCCCCCCCCCCCc  'C  C :'\$\$\$b   .e\$\$\$\$\$F  4     \$\$\$\$                  
     `CCCCCCCCCCCCCC 'Cc 'L'L'\$\$\$\$\$\$\$\$\$\$\$\$\" < \$b  .d\$\$\$                   
      `CCCCCCCCCCCCC ''Cc C: < \"?\$\$\$\$\$\$F\" >~C:`?\$\$\$\$\$\$\"                   
        `?CCCCCCCCC  ~;CCc Cc -:. \"\"\"\"  : .:~C:.`\"?\$F                     
           `CCCCCC   ;CCCC 'CC:'`CCC~ :CC ~  'CC >                        
              `'`  ~ CCCCCC. `CCC;;;<CCCCCCCc.'                           
                     'CCCC`Cc `CCCCCCCCCCC`'CCCCC                         
                       CCC `CC: `CCCCCCCCCCc `'CCCc                       
                        `'   C `: `CCCCCCCCCCccc, `                       
                              '  '  `'CCCCCCCCCCCCCC'`                    
                                       ```~~~~```                         
\n",
      "Iago" => " 
                                     .                                    
                    ::'         :i!!`                                     
                  .!'        :i!!                                         
                 i!        :!!!'                                          
               :!!        i!!                                             
               !!        :!'                                              
    :!        !!'       !!                                                
   i!         !!         !!  .. .                                         
   !'         !!       ::.::i!i ;!i                                       
   !          `!i   :!!!!!!!!!! ;!                     .,xnx,.            
   !           `!i: !!!!!!!'` '                    ..nMMMMMMMMMMn,        
   !!            ' :!!!!'  ....                 .dMMMMMMMMMMMMMMMMMk      
    `!i.          !;!!'   \"\"\"??\$b            ,nMMMMMMMMMMMMMMMMMMMMMM.    
     ``!!;;;;;;; f; !     ...  \"?b        ,-'\"\"\"MMMMMMMMMMMMMMMMMMMMMM.   
          ```  \!;    .F  \$F\"\"%  `?\$.   xP   nMMMMMMMMMMMMMMMMMMMMMMMM>   
               L!;    \$L    .....  `3F .MhnnnMMMMMMMMMMMMMMMMMMMMMMMMM>   
              ;!!!    \$\$b  '\$\$\$\$\$P\ 4  MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM>   
              ;!!!i   ^\$\$bu.          HMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM    
              ;!!!!i;  \"?\$\$\$b,   ... JMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMP    
               !!!!!!!:      ,;i!!!'  4MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM     
               ;!!!!!'`  .,i!!!!!'  .nMMMMMMMMMMMF??\"\"???MMMMMMMMMMM      
               :!!!'  ,i!!!!!!!'    `MMMMMMP?\"\"`          \"MMMMMMMM\"      
             .:!!' ,i!!!!!!!!'                  .,eec      MMMMMMM'       
           .:!!',i!!!!!!!!!'   ,\$;  \$\$; ,e\$\$\$; 4\$P\"`       MMMMMf         
         .!!!!!!!!!!!!!!!'     \$\$; \$\$\$; J\$\$\$\$; F\"          MMMP'          
       :!!!!!!!!!!!!!!!!   d  ;\$\$'.\$\$\$; \$F\"\"`             xM\"'            
     ''`` '!!!!!!!!!!!' ,, F      '\"\"\"`      .            \"               
          i!!!!!!!!!' .i!                   (                             
        i!'!!!!!!!! .i!!' >               .MM                             
       !'  !!!!!!!.i!!!' :                4M>                             
      /   '!!!!!!!!!!!'  f   ...:..       MM                              
     '    '!!!!'``!!!'  M>  .!!!!!~~      MM                              
          `!!!'  :!!!  4M   )!~`         4Mf                              
           '!'  <!!!'  Mf  '             4MP                              
               :!!!!   M                 4MM                              
             .!!!!!!  4M                 4MM                              
            ;!!!!!!'  dM.                'MML                             
          .!!!!!!!!   MML                 MMM.                            
        .!!!!!!!!!!   MMM.                 ?P\"                            
      ;!!!!!!!!!!!!   MMMM                    .xnMf                       
    ;!!!!!!!!!!!!!!   MMMMM.              ..nMMMMf                        
 ;!!!!!!!!!!!!!!!!!i  ?MMMMMMMn......xnnMMMMMMMMf                         
!!!!!!!!!!!!!!!!!!!!i  MMMMMMMMMMMMMMMMMMMMMMMM\"                          
!!!!!!!!!!!!!!!!!!!!!i  ?MMMMMMMMMMMMMMMMMMMF\"                            
!!!!!!!!!!!!!!!!!!!!!!i   \"?MMMMMMMMMMMMMF\"                               
!!!!!!!!!!!!!!!!!!!!!!!!;   `??MMMMMMP\"\"                                  
!!!!!!!!!!!!!!!!!!!!!!!!!       `\"\"`                                      
!!!!!!!!!!!!!!!!!!!!!!!!!i                                                
!!!!!!!!!!!!!!!!!!!!!!!!!!                                                
!!!!!!!!!!!!!!!!!!!!!!!!!!!                                               
!!!!!!!!!!!!!!!!!!!!!!!!!!!i                                              
\n",
      "Sultan" => "                               ,,..;;;,,.                                 
                             .,,.   `!!!!!!:                              
                     .,;!!!!!!!!!!!!i::!!!!!!:                            
                 ..!!!!!!!!!!!!!!!!!!!!!!!!!!!!                           
               :!!!!!!!!!!!!!!!!!!!!!!!!!!i !!!!  !                       
             :!!!!!'` ...>     `'!!!!!!!!!!i !!!!:!!                      
            !!!!' .i!!!!'         `!!!!!!!!!i !!!!!!                      
           i!!' :!!!!!'       .ee\$  !!!!!!!!!i !!!!!! `\$\$ec               
           !!L:!!!!!'      ze\$\$\$\$\$\$u !!!!!!!!!. !!!!! `\$\$\$\$\$eu            
           !!!!!!'      .d\$\$\$\$\$\$\$\$\$\$i !!!!!!!!! `!!!! `\$\$\$\$\$\$\$\$c          
            ```       z\$\$\$\$\$\$\$\$\$\$\$\$\$\$c`!!!!!!!!! !!!', :\$\$\$\$\$\$\$\$\$e        
                    u\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$. !!!!!!!!i`!!!! :\$\$\$\$\$\$\$\$\$\$\$.      
                  .\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$e !!!!!!!! !!!! 3\$\$\$\$\$\$\$\$\$\$\$\$c     
                 z\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$b `!!!!!! !!!  \$\$\$\$\$\$\$\$\$\$\$\$\$\$b    
                d\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$, `!!!!! !!   \$\$\$\$\$\$\$\$\$\$\$\$\$\$\$,   
               d\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$  )!!!! !!: .\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$   
            z x\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$b.`!!!! `!`z\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$h  
          d\$F \$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$br       ?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$F  
       .d\$\$\$r'\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\"  :L .h '\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$'  
      J\$\$\$\$\$&'\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$F: ~ ...  .\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$P   
    .\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$-'<.`(    \"???\$\$\$\$\$\$\$\$\$\$\$\$\$    
   x\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\"  ::...:: zue- 3\$\$\$\$\$\$\$\$\$\$P'    
   \$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$P\"    .e.. .er4?\"^\"'\$\$\$\$\$\$\$\$\$\$F      
  d\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$F\"\" .~~` =\$P?? \$\$k=\$\$F d\$\$\$\$\$\$F?\"\"       
 `\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$F\"\"\",.  >~ zeb /\"\$??\$\$\"\$??\$k .\"\"\"Lu-           
 `\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$E\"\"\"\" .z\$\$P\".cz-F' `3;F' `3\$e.`?\$eee..        
  ?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$r \"3\$\$\$\$\".e\$?? \$,,,d;\$,,,d\$\$??= \$\$\$\$\$L.      
   \"\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$F\".\$\$\$\$\$\$\"J\$\$\$\$\$bc.`'\$\$\$\$\$r ,ec,.d\$\$\$\$\$\$\$b.    
     \"R\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\"x\$\$\$\$\$\$\$\$>\" ueeeec.=??\$\$\$\$\" \$\$\$\$L. R\$F\"^\$\$F    
       \"?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$F ??\$\$??\$P\",e\$\$\$\$\$\$\$\$\$\$b`\"?\".d\$\$\$\$\$\$\$eee@ 3\$F    
          `\"?\$\$\$\$\$\$\$\$\$\$\$\$F 4\$\$\"qc,z\$\$\$\$\$\$\$\$\$F3P?)\$\$b\"\")\$\$\$\$\$\$\$\$\$\$P J\$F    
                \"\"???????? 3\$\$u\"\"??\$\$\$\$\$\$\$F\"zu <:> d eec,`\"\"\"\"\"\",zd?F     
                           3\$\$\$\$\$\$L\"????\".e\$\$\$i `l R'\$\$\$\$\$\$\$\$\$\$\$\$\$\$       
                            ? ?\$\$\$\$\$bedd\$\$\$\$\$\$E?ezd\",\$\$\$\$\$\$\$\$\$\$\$\$ F       
                               \"\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$beee\$\$\$\$\$\$\$\$\$\$\$\$\$P         
                                 \"\"?\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$F         
                                     \"?\$\$\$??\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$P          
                                        ^\"\" \"??\$\$\$\$\$\$\$\$\$\$\$\$\$\$F\"           
                                                `\"\"\"\"\"\"\"\"\"\"' \n",

      );

    $persons = array_keys($recon);
    $person = $persons[mt_rand(0, count($persons) - 1)];
    $names = implode(',', $persons);

    $question = 
"Who is this? [$names]\n
${recon[$person]}
";

    fwrite($this->connection, $question);
    sleep(1);
    $res = trim(fread($this->connection, 20));
    return $res == $person;

  }

  private function generateRandomString($length = 10) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, strlen($characters) - 1)];
      }
      return $randomString;
  }


  private function step3(){
    $x = mt_rand(1, 10);
    $y = $this->generateRandomString(15);
    $code = array(
      "php" => array(
        "0" => "
        \\\$a = '';
        for(\\\$i = 0; \\\$i < $x; \\\$i++){
          \\\$b = str_repeat('$y', $x);
          \\\$a .= \\\$b;
        }
        echo \\\$a;
        ",
        "1" => "
        \\\$b = 1;
        for(\\\$i = 0; \\\$i < strlen('$y'); \\\$i++){
          \\\$b *= $x;
        }
        echo \\\$b;
        ",
        "2" => "
        echo str_repeat($x, strlen('$y'));
        ",
        ),
      "python" => array(
        "0" => "print ''.join([x*(ord(x) % $x) for x in '$y'])",
        "1" => "print 'NaN'.join([x*(ord(x) % $x) for x in '$y'])",
        "2" => "print '$y'.join([str(x)*(ord(chr(x)) % $x) for x in range($x)])",),
      );
    $langs = array( 'php' , 'python');
    $lang = $langs[mt_rand(0,count($langs) -1 )];
    $task = $code[$lang][mt_rand(0,2)];
    $sub = $lang == 'php' ? '-r' : '-c';
    $question =  "$lang $sub \"$task\"\n";
    exec("$lang $sub \"$task\"", $out);
    
    fwrite($this->connection, $question);
    sleep(1);
    $res = trim(fread($this->connection, 2000));
    return $res == $out[0];
  }

  private function putFlag(){
    $message = "Ok, slave, now you can keep my secret 
Give me back this ".$this->flag." when I say the magic word ".md5($this->flag_id)."
";
    fwrite($this->connection, $message);
    return TRUE;
  }

  private function getFlag(){
    $message = "Ok, slave, now you can give my secret back. 
The magic word is ".md5($this->flag_id)."
";  
    fwrite($this->connection, $message);
    sleep(1);
    $res = trim(fread($this->connection, 40));
    return $res == $this->flag;

  }

	protected function _push($endpoint, $flagID, $flag)
	{
		$this->construct($endpoint . ':23232', $flagID, $flag);
			//isUp ?
		if( !$this->isUp())
			return array(Result::DOWN, $flagID);

		if( !$this->step1())
			return array(Result::MUMBLE, $flagID);

		if( !$this->step2())
			return array(Result::MUMBLE, $flagID);

		if( !$this->step3())
			return array(Result::MUMBLE, $flagID);

		if( !$this->putFlag())
			return array(Result::MUMBLE, $flagID);

		return array(Result::OK,$flagID);

	}

	protected function _pull($endpoint, $flagID, $flag)
    	{ 	//isUp ?
    		$this->construct($endpoint . ':23232', $flagID, $flag);
    		if( !$this->isUp())
    			return Result::DOWN;

    		if( !$this->step1())
    			return Result::MUMBLE;

    		if( !$this->step2())
    			return Result::MUMBLE;

    		if( !$this->step3())
    			return Result::MUMBLE;

			//check flag
    		if( !$this->getFlag())
    			return Result::CORRUPT;

    		return Result::OK;
    	}

    }

    $checker = new SolarssChecker();
    $checker->run();


